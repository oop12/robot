/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';
    
    public Robot(int x,int y,int bx,int by,int N){
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    @Override
    public String toString(){
        return "Robot("+this.x+","+this.y+") ("+Math.abs(x-bx)+","+Math.abs(y-by)+")";
    }
    public boolean inMap(int x,int y){
        if(x<0 || x>=N || y<0 || y>=N){
            System.out.println("I can't move!!!");
            return false;
        }
        return true;
    }
    public boolean isBomb(int x,int y){
        if(x==bx && y==by){
           System.out.println("Bomb found!!!");
           return true;
            }
        return false;
    }
    public boolean walk(char direction){
        switch(direction){
            case 'N': case 'n': 
                if(!inMap(x,y-1)){ return false;}y = y-1; break;
            case 'S': case 's': 
                if(!inMap(x,y+1)){ return false;}y = y+1; break;
            case 'E': case 'e':
                if(!inMap(x+1,y)){ return false;}x = x+1; break;
            case 'W': case 'w':
                if(!inMap(x-1,y)){ return false;}x = x-1; break;
        }
        lastDirection = direction;
        if(!isBomb(x,y)){}
        return true;
    }
    public boolean walk(char direction,int step){
        for(int i=0; i<step; i++){
            if(!Robot.this.walk(direction)){
                return false;}
        }
        return true;
    }
    public boolean walk(){
        return Robot.this.walk(lastDirection);
    }
    public boolean walk(int step){
        return Robot.this.walk(lastDirection,step);
    }
 

    
}
